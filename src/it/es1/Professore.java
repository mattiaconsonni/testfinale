package it.es1;

import java.util.HashSet;
import java.util.Set;

public class Professore {
    private String nome;
    private String cognome;
    private int id;
    private Set<Corso> corsiErogati;

    public Professore(String nome, String cognome, int id) {
        this.nome = nome;
        this.cognome = cognome;
        this.id = id;
        this.corsiErogati = new HashSet<>();
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return this.cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Corso> getCorsiErogati() {
        return this.corsiErogati;
    }

    public void setCorsiErogati(Set<Corso> corsiErogati) {
        this.corsiErogati = corsiErogati;
    }

    public void aggiungiCorso(Corso corso) {
        this.corsiErogati.add(corso);
    }

    public void rimuoviCorso(Corso corso) {
        this.corsiErogati.remove(corso);
    }

}
