package it.es1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Corso {

    private String titolo;
    private int id;
    private Professore professore;
    private Set<Studente> studenti;
    private int capienza;

    public Corso(String titolo, int id, Professore professore, int capienza) {
        this.titolo = titolo;
        this.id = id;
        this.professore = professore;
        this.studenti = new HashSet<>();
        this.capienza = capienza;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Professore getProfessore() {
        return this.professore;
    }

    public void setProfessore(Professore professore) {
        this.professore = professore;
    }

    public Set<Studente> getStudenti() {
        return this.studenti;
    }

    public int getCapienza() {
        return this.capienza;
    }

    public void setCapienza(int capienza) {
        this.capienza = capienza;
    }
    public void rimuoviStudente(Studente studente) {
        this.studenti.remove(studente);
        studente.rimozioneCorso(this);
    }

    public void aggiungiStudente(Studente studente) {
        if (this.studenti.size() > this.capienza || this.studenti.contains(studente))
            throw new IllegalArgumentException();
        this.studenti.add(studente);
        studente.iscrizioneCorso(this);
    }

    public void assegnazioneProfessore(Professore professore) {
        this.professore = professore;
        professore.aggiungiCorso(this);
    }
}
