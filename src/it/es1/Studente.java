package it.es1;

import java.util.HashSet;
import java.util.Set;

public class Studente {

    private String nome;
    private String cognome;
    private int matricola;
    private int annoImmatricolazione;
    private Set<Corso> corsiSeguiti;
    public Studente(String nome, String cognome, int matricola, int annoImmatricolazione) {
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
        this.annoImmatricolazione = annoImmatricolazione;
        this.corsiSeguiti = new HashSet<>();
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return this.cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getMatricola() {
        return this.matricola;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public int getAnnoImmatricolazione() {
        return this.annoImmatricolazione;
    }

    public void setAnnoImmatricolazione(int annoImmatricolazione) {
        this.annoImmatricolazione = annoImmatricolazione;
    }

    public Set<Corso> getCorsiSeguiti() {
        return this.corsiSeguiti;
    }

    public void setCorsiSeguiti(Set<Corso> corsiSeguiti) {
        this.corsiSeguiti = corsiSeguiti;
    }

    public void iscrizioneCorso(Corso corso) {
        this.corsiSeguiti.add(corso);
        corso.aggiungiStudente(this);
    }

    public void rimozioneCorso(Corso corso) {
        this.corsiSeguiti.remove(corso);
        corso.rimuoviStudente(this);
    }
}
