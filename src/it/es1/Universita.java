package it.es1;

import java.util.ArrayList;
import java.util.List;

public class Universita {
    private List<Studente> studenti;
    private List<Professore> professori;
    private List<Corso> corsi;

    public Universita() {
        this.studenti = new ArrayList<>();
        this.corsi = new ArrayList<>();
        this.professori = new ArrayList<>();
    }

    public void register(Studente studente) {
        this.studenti.add(studente);
    }

    public void register(Professore professore) {
        this.professori.add(professore);
    }

    public void creaCorso(String titolo, int id, Professore professore, int capienza) {
        this.corsi.add(new Corso(titolo, id, professore, capienza));
    }

    public void iscrizioneStudente(Studente studente, Corso corso) {
        studente.iscrizioneCorso(corso);
    }

    public void assegnaProfessore(Professore professore, Corso corso) {
        corso.assegnazioneProfessore(professore);
    }

    public void stampaStudentiCorso(Corso corso) {
        for (Studente studente: corso.getStudenti()) {
            System.out.println(studente.getNome());
        }
    }

    public void stampaCorsiProfessore(Professore professore) {
        for (Corso corsi: professore.getCorsiErogati()) {
            System.out.println(corsi.getTitolo());
        }
    }

    public boolean ricercaStudente(int matricola) {
        for (Studente studente: this.studenti) {
            if (studente.getMatricola() == matricola)
                return true;
        }
        return false;
    }

    public boolean ricercaProfessore(int id) {
        for (Professore professore: this.professori) {
            if (professore.getId() == id)
                return true;
        }
        return false;
    }

    public boolean ricercaCorsi(int id) {
        for (Corso corso: this.corsi) {
            if (corso.getId() == id)
                return true;
        }
        return false;
    }

}
